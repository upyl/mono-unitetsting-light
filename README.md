# README #

It is the fork of [Mono Droid Unit testing Framework](https://bitbucket.org/mayastudios/monodroid-unittest). But it uses NUnit attributes for testing and it is without GUI.
Steps to use:

* Download the source code
* Include the fodler 'Runner' to your Xamarin.IOS or Xamamrin.Android unit test projects. Include standard version of NUnit Light framework from Xamarin.
* For IOS in your controller:

```
#!c#

public class MyViewController : UIViewController
	{
		UITextView textView;

		public MyViewController()
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var runner = new TestRunner();
			// Run all tests from this assembly
			runner.AddTests(Assembly.GetExecutingAssembly());
			var log = new StringBuilder();

			View.Frame = UIScreen.MainScreen.Bounds;
			View.BackgroundColor = UIColor.White;
			View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;

			textView = new UITextView(new RectangleF(
				0,
				0,
				View.Frame.Width,
				View.Frame.Height));

			textView.ScrollEnabled = true;
			
			ThreadPool.QueueUserWorkItem(delegate
			{
				runner.RunTests(new TestHandler(log));
				this.BeginInvokeOnMainThread(delegate
				{
					textView.Text = log.ToString();
				});
			});
			
			textView.Text = "Test";
			View.AddSubview(textView);
		}

	}
```
* For Android:

```
#!c#

[Activity(Label = "MonoDroidUnitTesting Example", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			var runner = new TestRunner();
			// Run all tests from this assembly
			runner.AddTests(Assembly.GetExecutingAssembly());

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			var log = new StringBuilder();

			try
			{
				runner.RunTests(new TestHandler(log));
			}
			catch (Exception ex)
			{
				log = new StringBuilder(ex.ToString());
			}

			TextView tv = FindViewById<TextView>(Resource.Id.textView1);
			tv.Text = log.ToString();
		}
	}
```
* It is all. Run your unit tests.

### What is this repository for? ###

* Unit testing in Xamamrin
* 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Who do I talk to? ###

* Eugene Pyl (gromkaktus@gmail.com)
* All rights are in [MonoDroid](https://bitbucket.org/mayastudios/monodroid-unittest)