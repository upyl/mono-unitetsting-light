//
// Copyright (C) 2012 Maya Studios (http://mayastudios.com)
//
// This file is part of MonoDroidUnitTesting.
//
// MonoDroidUnitTesting is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MonoDroidUnitTesting is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with MonoDroidUnitTesting. If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework;
using System.Threading.Tasks;

namespace MonoDroidUnitTesting
{
    /// <summary>
    ///     Represents a collection of test methods.
    /// </summary>
    public class TestClass
    {
        private static readonly int OUTCOME_TYPE_COUNT = Enum.GetValues(typeof (TestState)).Length;
        private readonly Dictionary<TestState, int> m_stateCount = new Dictionary<TestState, int>();
        private readonly SortedDictionary<string, TestMethod> m_testMethods = new SortedDictionary<string, TestMethod>();

        internal TestClass(Type @class, bool addTestMethods)
        {
            Class = @class;
            Reset();

            foreach (MethodInfo method in @class.GetMethods())
            {
                if (!IsValidTestMethod(method))
                {
                    // Invalid method
                    continue;
                }

                if (method.GetCustomAttributes(typeof (TestAttribute), true).Length != 0)
                {
                    if (addTestMethods)
                    {
                        AddTestMethod(method);
                    }
                }
                else if (method.GetCustomAttributes(typeof (SetUpAttribute), true).Length != 0)
                {
                    SetupMethod = method;
                }
                else if (method.GetCustomAttributes(typeof (TearDownAttribute), true).Length != 0)
                {
                    TeardownMethod = method;
                }
                else if (method.GetCustomAttributes(typeof (TestFixtureSetUpAttribute), true).Length != 0)
                {
                    ClassSetupMethod = method;
                }
				else if (method.GetCustomAttributes(typeof(TestFixtureTearDownAttribute), true).Length != 0)
                {
                    ClassTeardownMethod = method;
                }
            }
        }

        public Type Class { get; private set; }

        public IEnumerable<TestMethod> TestMethods
        {
            get
            {
                foreach (var item in m_testMethods)
                {
                    yield return item.Value;
                }
            }
        }

        public int TestMethodCount
        {
            get { return m_testMethods.Count; }
        }

        public MethodInfo ClassSetupMethod { get; private set; }
        public MethodInfo ClassTeardownMethod { get; private set; }
        public MethodInfo SetupMethod { get; private set; }
        public MethodInfo TeardownMethod { get; private set; }

        public TestState State { get; private set; }

        public Exception TestClassError { get; private set; }
        public TestClassErrorType? ErrorType { get; private set; }

        private static bool IsValidTestMethod(MethodInfo method)
        {
            return (!method.IsAbstract && !method.IsConstructor && !method.IsGenericMethod && method.GetParameters().Length == 0 && method.IsPublic);
        }

        internal void AddAllTestMethods()
        {
            foreach (MethodInfo method in Class.GetMethods())
            {
                if (!IsValidTestMethod(method))
                {
                    // Invalid method
                    continue;
                }

                if (method.GetCustomAttributes(typeof (TestAttribute), true).Length != 0)
                {
                    AddTestMethod(method);
                }
            }
        }

        internal void AddTestMethod(MethodInfo method)
        {
            if (method.DeclaringType != Class)
            {
                throw new ArgumentException();
            }

            if (m_testMethods.ContainsKey(method.Name))
            {
                return; // just ignore this
            }

            if (!IsValidTestMethod(method))
            {
                throw new ArgumentException("Method " + method.Name + " is not a valid test method.");
            }

            var willBeIgnored = (method.GetCustomAttributes(typeof(IgnoreAttribute), true).Length != 0);

            m_testMethods.Add(method.Name, new TestMethod(this, method, willBeIgnored));
            m_stateCount[TestState.NotYetRun] = m_stateCount[TestState.NotYetRun] + 1;
        }

        internal void Run(ITestResultHandler resultHandler, int testClassIndex)
        {
            State = TestState.Running;

            resultHandler.OnTestClassTestStarted(this, testClassIndex);

            object testClassInstance;
			try
			{
				testClassInstance = Task.Run(() => Activator.CreateInstance(Class)).Result;
			}
			catch (AggregateException e)
			{
				if (e.InnerException is TargetInvocationException && e.InnerException.InnerException != null)
				{
					TestClassError = e.InnerException.InnerException;
				}
				else
				{
					TestClassError = e.InnerException;
				}

				State = TestState.Failed;
				ErrorType = TestClassErrorType.ConstructorError;

				resultHandler.OnTestClassError(this, testClassIndex);
				return;
			}
			catch (Exception e)
			{
				if (e is TargetInvocationException && e.InnerException != null)
				{
					e = e.InnerException;
				}

				State = TestState.Failed;
				TestClassError = e;
				ErrorType = TestClassErrorType.ConstructorError;

				resultHandler.OnTestClassError(this, testClassIndex);
				return;
			}

            if (ClassSetupMethod != null)
            {
                Exception e = TestMethod.InvokeTestMethod(testClassInstance, ClassSetupMethod);
                if (e != null)
                {
                    State = TestState.Failed;
                    TestClassError = e;
                    ErrorType = TestClassErrorType.ClassInitializerError;

                    resultHandler.OnTestClassError(this, testClassIndex);
                    return;
                }
            }

            int testMethodIndex = 1;
            m_stateCount[TestState.Running] = 1;
            foreach (TestMethod testMethod in TestMethods)
            {
                testMethod.Run(testClassInstance, testMethodIndex, resultHandler);

                m_stateCount[testMethod.State] = m_stateCount[testMethod.State] + 1;
                m_stateCount[TestState.NotYetRun] = m_stateCount[TestState.NotYetRun] - 1;

                testMethodIndex++;
            }
            m_stateCount[TestState.Running] = 0;

            if (ClassTeardownMethod != null)
            {
                Exception e = TestMethod.InvokeTestMethod(testClassInstance, ClassTeardownMethod);
                if (e != null)
                {
                    State = TestState.Failed;
                    TestClassError = e;
                    ErrorType = TestClassErrorType.ClassCleanupError;

                    resultHandler.OnTestClassError(this, testClassIndex);
                    return;
                }
            }

            if (GetStateCount(TestState.Failed) != 0)
            {
                State = TestState.Failed;
            }
            else if (GetStateCount(TestState.Inconclusive) != 0)
            {
                State = TestState.Inconclusive;
            }
            else if (GetStateCount(TestState.Ignored) != 0)
            {
                State = TestState.Ignored;
            }
            else
            {
                State = TestState.Passed;
            }

            resultHandler.OnTestClassTestEnded(this, testClassIndex);
        }

        public int GetStateCount(TestState outcomeType)
        {
            return m_stateCount[outcomeType];
        }

        internal void Reset()
        {
            foreach (TestMethod method in TestMethods)
            {
                method.Reset();
            }

            for (int i = 0; i < OUTCOME_TYPE_COUNT; i++)
            {
                m_stateCount[(TestState) i] = 0;
            }
            m_stateCount[TestState.NotYetRun] = TestMethodCount;

            State = TestState.NotYetRun;
            TestClassError = null;
            ErrorType = null;
        }

        public TestMethod GetTestMethod(string name)
        {
            TestMethod testMethod;
            if (m_testMethods.TryGetValue(name, out testMethod))
            {
                return testMethod;
            }
            return null;
        }

        public List<TestMethod> GetTestMethodsSorted(Comparison<TestMethod> comparer)
        {
            var sorted = new List<TestMethod>(m_testMethods.Values);
            sorted.Sort(comparer);
            return sorted;
        }
    }

    public enum TestClassErrorType
    {
        ConstructorError,
        ClassInitializerError,
        ClassCleanupError
    }
}