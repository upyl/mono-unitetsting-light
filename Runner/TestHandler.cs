using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoDroidUnitTesting
{
	public class TestHandler : ITestResultHandler
	{
		private int Failed = 0;
		private int Passed = 0;
		private int Ignored = 0;

		private StringBuilder builder;

		public TestHandler(StringBuilder builder)
		{
			this.builder = builder;
		}

		public void OnTestRunStarted(TestRunner runner)
		{
			builder.AppendLine("OnTestRunStarted");
		}

		public void OnTestRunEnded(TestRunner runner)
		{
			builder.AppendLine("OnTestRunEnded");
			builder.AppendFormat("Passed: {0}, Failed: {1}, Ignored: {2}", Passed, Failed, Ignored);
		}

		public void OnTestClassTestStarted(TestClass testClass, int testClassIndex)
		{
			builder.AppendFormat("Class {0} - {1} started\n", testClass.Class.Name, testClassIndex);
		}

		public void OnTestClassError(TestClass testClass, int testClassIndex)
		{
			builder.AppendFormat("Class {0} - {1} has error : {2}\n", testClass.Class.Name, testClassIndex, testClass.TestClassError);
		}

		public void OnTestClassTestEnded(TestClass testClass, int testClassIndex)
		{
			builder.AppendFormat("Class {0} - {1} ended\n", testClass.Class.Name, testClassIndex);
		}

		public void OnTestMethodStarted(TestMethod testMethod, int testMethodIndex)
		{
			builder.AppendFormat("Method {0} - {1} started\n", testMethod.Method.Name, testMethodIndex);
		}

		public void OnTestMethodEnded(TestMethod testMethod, int testMethodIndex)
		{
			builder.AppendFormat("Method {0} - {1} ended. Result {2}\n", testMethod.Method.Name, testMethodIndex, testMethod.State);
			if (testMethod.OutcomeError != null)
			{
				builder.AppendFormat("Error: {0}\n{1}\n", testMethod.OutcomeError.Message, testMethod.OutcomeError.Exception);
			}
			if (testMethod.State == TestState.Failed)
			{
				Failed += 1;
			}
			if (testMethod.State == TestState.Passed)
			{
				Passed += 1;
			}
			if (testMethod.State == TestState.Ignored)
			{
				Ignored += 1;
			}
		}
	}
}