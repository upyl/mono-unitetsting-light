//
// Copyright (C) 2012 Maya Studios (http://mayastudios.com)
//
// This file is part of MonoDroidUnitTesting.
//
// MonoDroidUnitTesting is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// MonoDroidUnitTesting is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with MonoDroidUnitTesting. If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Reflection;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MonoDroidUnitTesting
{
    /// <summary>
    ///     Represents a test method.
    /// </summary>
    public class TestMethod : MethodOutcome
    {
        private static readonly object[] NO_PARAMS = new object[0];

        private readonly bool _willBeIgnored;

        internal TestMethod(TestClass testClass, MethodInfo method, bool willBeIgnored) : base(method)
        {
            Class = testClass;
            this._willBeIgnored = willBeIgnored;
        }

        /// <summary>
        ///     The test class this test method belongs to. Never null.
        /// </summary>
        public TestClass Class { get; private set; }

        /// <summary>
        ///     Usually this method shouldn't be invoked directly. Use <see cref="TestClass.Run" /> instead. If no test class has
        ///     been specified in the constructor, this method only executes the method itself (but no class setup or teardown
        ///     methods).
        /// </summary>
        public void Run(object testClassInstance, int testMethodIndex, ITestResultHandler resultHandler)
        {
            State = TestState.Running;

            resultHandler.OnTestMethodStarted(this, testMethodIndex);

            Exception outcomeError = null;

            if (!this._willBeIgnored)
            {
                if (Class.SetupMethod != null)
                {
                    outcomeError = InvokeTestMethod(testClassInstance, Class.SetupMethod);
                }

                if (outcomeError == null)
                {
                    // No error in the setup method
                    outcomeError = InvokeTestMethod(testClassInstance, Method);
                    if (outcomeError != null && IsExpectedException(Method, outcomeError))
                    {
                        outcomeError = null;
                    }

                    if (Class.TeardownMethod != null)
                    {
                        Exception e = InvokeTestMethod(testClassInstance, Class.TeardownMethod);
                        // Make sure we don't override the actual failure reson.
                        if (e != null && outcomeError == null)
                        {
                            outcomeError = e;
                        }
                    }
                }

                SetOutcome(outcomeError);
            }
            else
                this.State = TestState.Ignored;

            resultHandler.OnTestMethodEnded(this, testMethodIndex);
        }

        private static bool IsExpectedException(MethodInfo method, Exception e)
        {
            object[] attributes = method.GetCustomAttributes(typeof (ExpectedExceptionAttribute), true);
            if (attributes.Length == 0)
            {
                return false;
            }

            Type type = e.GetType();

            foreach (ExpectedExceptionAttribute attribute in attributes)
            {
                if (type == attribute.ExpectedException)
                {
                    return true;
                }
            }

            return false;
        }

        internal static Exception InvokeTestMethod(object testClassInstance, MethodInfo method)
        {
			try
			{
				return Task.Run(async () =>
				{
					var returnTask = method.Invoke(testClassInstance, NO_PARAMS) as Task;

					if (returnTask != null)
					{
						try
						{
							await returnTask;
						}
						catch (AggregateException ex)
						{
							return ex.InnerException;
						}
					}
					return null;
				}).Result;
			}
			catch (AggregateException e)
			{
				if (e.InnerException is TargetInvocationException)
				{
					return e.InnerException.InnerException ?? e.InnerException;
				}
				return e.InnerException;
			}
			catch (TargetInvocationException e)
			{
				return e.InnerException ?? e;
			}
			catch (Exception e)
			{
				return e;
			}
        }
    }
}